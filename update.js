const fs = require('fs');
const path = require('path');
const md5 = require('md5-file');

function getHashes(dir) {
    const hashes = {};

    fs.readdirSync(dir).forEach(name => {
        const p = path.join(dir, name);
        const stat = fs.lstatSync(p);
        if (stat.isDirectory()) {
            hashes[name] = getHashes(p);
        } else {
            hashes[name] = md5.sync(p);
        }
    });

    return hashes;
}

const hashes = getHashes('src');
delete hashes['versions.lua'];
const lua = 'VERSIONS = ' + JSON.stringify(hashes, null, 2)
    .split('\n').map(line => {
        const spl = line.split(':');
        if (spl.length === 2) {
            return spl[0].replace(/\"/g, '').replace(/\.lua/g, '') + ' =' + spl[1];
        } else {
            return line;
        }
    }).join('\n');
fs.writeFileSync('src/versions.lua', lua);