function req(scriptFile)
    local env = {
        require = require
    }
    for k, v in pairs(_G) do
        env[k] = v
    end
    local _, result = assert(pcall(setfenv(assert(loadfile(scriptFile .. '.lua')), env)))
    return result, env
end