local function recharge()
	print("Depositing items & refueling")
	
	turtle.digUp()  -- make space for ender chest
	turtle.select(1)
	if not turtle.placeUp() then
		return false
	end
	
	if turtle.getFuelLevel() < 1000 then
		turtle.suckUp()
		turtle.refuel()
	end
	
	for i = 16,1,-1 do
		turtle.select(i)
		turtle.dropUp()
	end
	
	turtle.digUp()
end