local Wireless = require('/code/net/wireless')

local w = Wireless:create()

function on_message(data, reply)
    print("Got response " .. data.message .. " at distance " .. data.distance)
end

function main()
    while true do
        print("Channel:")
        local channel = tonumber(read())
        print("Message:")
        local message = read()
        w:send(channel, 999, message)
    end
end

w:addListener(on_message, 999)
w:listen(main)