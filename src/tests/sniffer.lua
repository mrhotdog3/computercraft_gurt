local inspect = require('/code/inspect')
local Util = require('/code/util')

local modem = Util:openPeripheral("modem")

if modem == nil then
    print("Could not find modem")
    return
end

-- args = {...}
print("Channels:")
local args = Util:split(read(), " ")
local i = 1
while args[i] do
    print("Listening on " .. args[i])
    modem.open(tonumber(args[i]))
    i = i + 1
end

while true do
    local _, _, channel, replyChannel, message, dist = os.pullEvent("modem_message")

    local log = "c" .. channel .. " | r" .. replyChannel .. " | m" .. inspect(message)
    print(log)
    local f = fs.open("sniff.log", "a")
    f.write(log .. "\n")
    f.close()
end