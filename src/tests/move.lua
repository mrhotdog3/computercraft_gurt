local Gurt = require('/code/gurt')
local inspect = require('/code/inspect')
local C = require('/code/consts')

g = Gurt:create()
if g:getLocation() == nil then
    g:setLocationFromUser()
end
print("starting at " .. inspect(g:getLocation()))
g:moveTo({
    x = 0,
    y = 0,
    z = 0,
    h = C.NORTH
})
print("ended at " .. inspect(g:getLocation()))