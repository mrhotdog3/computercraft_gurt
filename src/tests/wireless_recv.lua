local Wireless = require('/code/net/wireless')

local w = Wireless:create()

function gen_cb(k)
    function on_message(data, reply)
        print("[" .. k .. "] Got message " .. data.message .. " at distance " .. data.distance)
        reply("Thank you " .. k)
    end

    return on_message
end

function main()
    print("Listening!")
end

w:addListener(gen_cb("111 & 222"), 111, 222)
w:addListener(gen_cb("333"), 333)
w:listen(main)