local Network = require('/code/net/network')
local GPS = require('/code/net/gps')

local n = Network:create()
local gps = GPS:create(n)

function main()
    print("Locating...")

    local loc = gps:locate()

    print("Location: " .. loc)
end

n:listen(main)