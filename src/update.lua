BASE_URL = "https://bitbucket.org/mrhotdog3/computercraft_gurt/raw/master/src"

function downloadFile(filePath)
    print("Downloading " .. filePath)
    local i = nextInt()
    local req = http.get(BASE_URL .. filePath .. "?t=" .. i)
    local file = fs.open(filePath, "w")
    file.write(req.readAll())
    file.close()
    req.close()
end

function nextInt()
    if fs.exists("/update_int") then
        local f = fs.open("/update_int", "r")
        local val = f.readAll()
        f.close()
        f = fs.open("/update_int", "w")
        f.write(tostring(tonumber(val) + 1))
        f.close()
        return val
    end

    math.randomseed(os.time())
    local val = math.floor(math.random() * 100000)
    local f = fs.open("/update_int", "w")
    f.write(tostring(val + 1))
    f.close()

    return val
end

function updateDir(cur_v, new_v, dir)
    if cur_v == nil then
        cur_v = {}
    end
    for name, val in pairs(new_v) do
        local path = dir .. name
        if type(val) == "string" then
            -- Check if we need to update
            if cur_v[name] ~= val then
                downloadFile(path .. ".lua")
            end
        else
            updateDir(cur_v[name], val, path .. "/")
        end
        cur_v[name] = nil
    end

    -- Delete all extra files
    for name, val in pairs(cur_v) do
        if type(val) == "string" then
            name = name .. ".lua"
        end
        local path = dir .. name
        print("Deleting " .. path)
        fs.delete(path)
    end
end

-- Download latest versions
downloadFile("/versions.lua")

-- Load new versions
require("/versions")
new_versions = VERSIONS

-- Load current versions
if fs.exists("/cur_versions.lua") then
    require("/cur_versions")
    cur_versions = VERSIONS
else
    cur_versions = {}
end

updateDir(cur_versions, new_versions, "/")

fs.delete("/cur_versions.lua")
fs.copy("/versions.lua", "/cur_versions.lua")