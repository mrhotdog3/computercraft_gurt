local Persist = require('/code/persist')
local Util = require('/code/util')
local inspect = require('/code/inspect')
local C = require('/code/consts')

--[[

loc: {x, y, z, h}
    x,y,z   - coords
    h       - heading

--]]

local Gurt = {}
Gurt.__index = Gurt

function Gurt:create()
    local gurt = {}
    setmetatable(gurt, Gurt)

    gurt.persist = Persist:create("gurt")

    return gurt
end

function Gurt:getLocation() return self.persist:get("location") end

function Gurt:_setLocation(loc) self.persist:set("location", loc) end

function Gurt:setLocation000()
    self:_setLocation({
        x = 0,
        y = 0,
        z = 0,
        h = C.NORTH
    })
end

function Gurt:setLocationFromUser()
    print("Stand on top of me and tell me your location.\n" ..
              "Round Y up, Drop X/Z decimal.\n" .. "H = N | S | E | W\n" ..
              ".Format: X,Y,Z,H")
    local loc = Util:split(read(), ",")
    local valid = Util:length(loc) == 4
    loc = Util:map(loc, function(s, idx)
        if idx == 4 then return s end
        local res = tonumber(s)
        if res == nil then valid = false end
        return res
    end)
    if not valid then
        print("Invalid input")
        return
    end
    local newLoc = {
        x = loc[1],
        y = loc[2] - 1,
        z = loc[3],
        h = Util:headingFromString(loc[4])
    }
    if newLoc.x < 0 then newLoc.x = newLoc.x - 1 end
    if newLoc.z < 0 then newLoc.z = newLoc.z - 1 end
    self:_setLocation(newLoc)
end

function Gurt:rotateTo(heading)
    local cur = self:getLocation()
    if cur.h == heading then return cur end
    local cw = heading - cur.h
    if cw < 0 then cw = cw + 4 end
    local num = math.min(cw, 4 - cw)
    for i = 1, num do
        if cw > 2 then
            turtle.turnLeft()
        else
            turtle.turnRight()
        end
    end
    cur.h = heading
    self:_setLocation(cur)
    return cur
end

function Gurt:moveForward(amount, dig)
    if amount == nil then amount = 1 end
    if dig == nil then dig = false end
    local pos = self:getLocation()
    for i=1,amount do
        if dig then turtle.dig() end
        local success = turtle.forward()
        if success then
            pos = Util:addLoc(pos, Util:directionFromHeading(pos.h))
            self:_setLocation(pos)
        else
            return false
        end
    end
    return true
end

function Gurt:moveUp(amount, dig)
    if amount == nil then amount = 1 end
    if dig == nil then dig = false end
    local dir = Util:tern(amount > 0, 1, -1)
    if dir == -1 then
        amount = -amount
    end
    local pos = self:getLocation()
    for i=1,amount do
        if dig then
            if dir == 1 then turtle.digUp() else turtle.digDown() end
        end
        local success = Util:tern(dir == 1, turtle.up, turtle.down)
        if success then
            pos.y = pos.y + dir
            self:_setLocation(pos)
        else
            return false
        end
    end
    return true
end

function Gurt:moveTo(loc, dig)
    if dig == nil then dig = false end

    local checks = {}
    checks[C.EAST] = function (c) return loc.x - c.x end
    checks[C.WEST] = function (c) return c.x - loc.x end
    checks[C.SOUTH] = function (c) return loc.z - c.z end
    checks[C.NORTH] = function (c) return c.z - loc.z end

    for h, f in pairs(checks) do
        local cur = self:getLocation()
        local diff = f(cur)
        if diff > 0 then
            self:rotateTo(h)
            self:moveForward(diff, true)
        end
    end

    local cur = self:getLocation()
    self:moveUp(loc.y - cur.y, true)

    return self:rotateTo(loc.h)
end

return Gurt