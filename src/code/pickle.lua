require('/req')
local _, env = req('/code/polluted/pickle')
return {
    pickle = env.pickle,
    unpickle = env.unpickle
}