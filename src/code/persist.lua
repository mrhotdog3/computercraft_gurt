local Pickle = require('/code/pickle')

local Persist = {}
Persist.__index = Persist

function Persist:create(storeName)
    local p = {}
    setmetatable(p, Persist)
    p.dir = "/persist/" .. storeName .. "/"
    if not fs.exists(p.dir) then
        fs.makeDir(p.dir)
    end
    return p
end

function Persist:set(key, table)
    local file = fs.open(self.dir .. key, "w")
    file.write(Pickle.pickle(table))
    file.close()
end

function Persist:get(key)
    local file = fs.open(self.dir .. key, "r")
    if file == nil then
        return nil
    end
    local encoded = file.readAll()
    file.close()
    return Pickle.unpickle(encoded)
end

return Persist