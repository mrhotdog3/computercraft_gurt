return {
    NORTH = 0, -- -z
    EAST = 1,  -- +x
    SOUTH = 2, -- +z
    WEST = 3   -- -x
}