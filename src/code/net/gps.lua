local Topics = require('/code/net/topics')
local Packet = require('/code/net/packet')
local Util = require('/code/util')

local GPS = {}
GPS.__index = GPS

function GPS:create(network)
    local g = {}
    setmetatable(g, GPS)

    g.net = network
    g.net:addListener(Util:bind(g, '_netcb'), Topics.GPS_RESP)
    g.responses = {}

    return g
end

function GPS:_netcb(packet)
    table.insert(self.responses, packet)
end

function GPS:locate(network)
    -- Clear responses
    self.responses = {}

    -- Broadcast gps query
    self.net:broadcast_local(Packet:gps_query())

    -- Wait for responses
    while true do
        print("Num responses: " .. Util:length(self.responses))
        os.sleep(1)
    end
    return "a"
end

return GPS