-- Pretty much a wrapper for modem peripheral
local Util = require('/code/util')

local Wireless = {}
Wireless.__index = Wireless

function Wireless:create()
    local w = {}
    setmetatable(w, Wireless)

    w.modem = Util:openPeripheral("modem")
    if w.modem == nil then
        print("No modem found")
        return nil
    end

    w.listeners = {}

    return w
end

-- Wireless:addListener(cb_func, chan1, chan2, ...)
-- cb_func({message, distance}, reply_func)
function Wireless:addListener(...)
    if arg.n < 2 then
        print("Not enough arguments provided to addListener")
        return
    end

    local cb_func = arg[1]
    for i = 2, arg.n do
        self.listeners[arg[i]] = cb_func
    end
end

function Wireless:listen(async_func)
    local function _listen()
        while true do
            local _, _, channel, replyChannel, message, dist = os.pullEvent("modem_message")
            local function reply(replyMessage)
                self.modem.transmit(replyChannel, channel, replyMessage)
            end

            local listener = self.listeners[channel]
            if listener == nil then
                print("Received message on channel " .. channel .. " but no listener found")
            else
                listener({
                    message = message,
                    distance = dist
                }, reply)
            end
        end
    end

    for chan, _ in pairs(self.listeners) do
        if not self.modem.isOpen(chan) then
            self.modem.open(chan)
        end
    end

    if async_func == nil then
        _listen()
    else
        parallel.waitForAll(_listen, async_func)
    end
end

function Wireless:close(channel)
    if self.modem.isOpen(channel) then
        self.modem.close(channel)
    end
end

function Wireless:send(channel, replyChannel, message)
    self.modem.transmit(channel, replyChannel, message)
end

return Wireless