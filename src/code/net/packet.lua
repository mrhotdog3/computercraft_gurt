local Topics = require('/code/net/topics')

local Packet = {}
Packet.__index = Packet

local function build_packet(topic, data)
    if data == nil then
        data = {}
    end
    return {
        topic = topic,
        data = data
    }
end

function Packet:gps_query()
    return build_packet(Topics.GPS_QUERY)
end

function Packet:gps_resp(x, y, z)
    return build_packet(Topics.GPS_RESP, {
        x = x,
        y = y,
        z = z
    })
end

return Packet