local CHANNELS = {
    GLOBAL_REMOTE = 1,
    GLOBAL_LOCAL = 2
}

local function calc_channel(id)
    local BASE = 0
    local SPACE = 3
    local NUM_GLOBAL = 2

    if id == "l*" then
        return CHANNELS.GLOBAL_LOCAL
    elseif id == "r*" then
        return CHANNELS.GLOBAL_REMOTE
    else
        return BASE + NUM_GLOBAL + (id % (SPACE - NUM_GLOBAL))
    end
end

CHANNELS["calc_channel"] = calc_channel

return CHANNELS