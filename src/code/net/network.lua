local Wireless = require('/code/net/wireless')
local Channel = require('/code/net/channel')

local Network = {}
Network.__index = Network

function Network:create()
    local n = {}
    setmetatable(n, Network)

    n.id = tostring(os.getComputerID())
    n.channel = Channel.calc_channel(n.id)
    n.conn = Wireless:create()
    n.listeners = {}

    return n
end

-- Network:addListener(cb_func, topic1, topic2, ...)
-- If no topics are provided, will callback on any unhandled topic
-- cb_func(packet{sender, target, topic, distance, data}, reply_func)
function Network:addListener(...)
    if arg.n < 1 then
        print("Not enough arguments provided to Network:addListener")
        return
    elseif arg.n == 1 then
        self.listeners["*"] = arg[1]
    else
        for i = 2, arg.n do
            self.listeners[arg[i]] = arg[1]
        end
    end
end

function Network:listen(async_func)
    local function _cb(data)
        local packet = data.message
        packet.distance = data.distance

        -- Check that packet is intended for us
        if packet.target ~= self.id and packet.target ~= "l*" and packet.target ~= "r*" then
            return
        end

        local function _reply(reply_packet)
            self:send(packet.sender, reply_packet)
        end

        local listener = self.listeners[packet.topic] or self.listeners["*"]
        if listener ~= nil then
            listener(packet, _reply)
        end
    end

    self.conn:addListener(_cb,
        Channel.GLOBAL_REMOTE,
        Channel.GLOBAL_LOCAL,
        self.channel
    )

    self.conn:listen(async_func)
end

function Network:broadcast_local(packet)
    self:send("l*", packet)
end

function Network:broadcast_remote(packet)
    self:send("r*", packet)
end

function Network:send(target, packet)
    if type(target) == "number" then
        target = tostring(target)
    end
    if type(target) == "string" then
        target = {target}
    end

    packet.sender = self.id

    for _, t in pairs(target) do
        packet.target = t
        self.conn:send(Channel.calc_channel(t), self.channel, packet)
    end
end

return Network