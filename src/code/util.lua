local C = require('/code/consts')

local Util = {}
Util.__index = Util

function Util:split(inputstr, sep)
    if sep == nil then sep = "%s" end
    local t = {}
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        table.insert(t, str)
    end
    return t
end

function Util:map(table, func)
    local t = {}
    local i = 1
    for k, v in pairs(table) do
        t[k] = func(v, i)
        i = i + 1
    end
    return t
end

function Util:length(obj)
    local len = 0
    for _, _ in pairs(obj) do len = len + 1 end
    return len
end

function Util:headingFromString(s)
    if s == "n" or s == "N" then
        return C.NORTH
    elseif s == "s" or s == "S" then
        return C.SOUTH
    elseif s == "e" or s == "E" then
        return C.EAST
    elseif s == "w" or s == "W" then
        return C.WEST
    else
        error("Invalid heading: " .. s)
    end
end

function Util:directionFromHeading(h)
    local dir = {
        x = 0,
        y = 0,
        z = 0,
        h = h
    }

    if h == C.NORTH then
        dir.z = -1
    elseif h == C.SOUTH then
        dir.z = 1
    elseif h == C.EAST then
        dir.x = 1
    elseif h == C.WEST then
        dir.x = -1
    end
    
    return dir
end

function Util:addLoc(a, b)
    return {
        x = a.x + b.x,
        y = a.y + b.y,
        z = a.z + b.z,
        h = a.h
    }
end

function Util:distance(a, b)
    return math.abs(a.x - b.x) + math.abs(a.y - b.y) + math.abs(a.z - b.z)
end

function Util:tern(cond, a, b)
    if cond then
        if type(a) == "function" then
            return a()
        else
            return a
        end
    else
        if type(b) == "function" then
            return b()
        else
            return b
        end
    end
end

function Util:openPeripheral(type)
    local sides = peripheral.getNames()

    for k, side in pairs(sides) do
        if peripheral.getType(side) == type then
            return peripheral.wrap(side)
        end
    end

    return nil
end

function Util:bind(t, k)
    return function(...) return t[k](t, ...) end
end

return Util