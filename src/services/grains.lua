-- Script to automate creation of "Grains of Infinity" by lighting bedrock on fire and waiting for it to burn out
turtle.select(1)

while true do    
    if turtle.getItemCount() == 0 then
        turtle.suckUp(1)
    end

    if turtle.inspect() then
        turtle.turnRight()
    else
        turtle.place()
    end
end