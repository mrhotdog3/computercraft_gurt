local Util = require('/code/util')

local sides = peripheral.getNames()
if Util:length(sides) ~= 1 then
    print("Failed to find single peripheral")
    return
end

local p = peripheral.wrap(sides[1])

while true do
    local perc = p.getEnergyStored() / p.getMaxEnergyStored()

    if perc < 0.75 then
        redstone.setOutput("back", true)
    elseif perc > 0.95 then
        redstone.setOutput("back", false)
    end

    os.sleep(1)
end