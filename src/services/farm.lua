-- Turn to face air
while turtle.inspect() do
    turtle.turnRight()
end

function check()
    success, data = turtle.inspect()
    if success and data.metadata == 7 then
        turtle.dig()
        turtle.place()
    end
end

while true do
    -- Check left
    turtle.turnLeft()
    check()
    turtle.turnRight()

    -- Check right
    turtle.turnRight()
    check()
    turtle.turnLeft()

    -- Deposit if at chest
    local success, data = turtle.inspect()
    if success and string.match(data.name, "chest") then
        for i = 2, 16 do
            turtle.select(i)
            turtle.drop()
        end
        turtle.select(1)
    end

    -- Turn around if blocked
    if success then
        turtle.turnRight()
        turtle.turnRight()
    end

    -- Move forward
    turtle.forward()
end