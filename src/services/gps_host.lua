local Network = require('/code/net/network')
local Topics = require('/code/net/topics')
local Packet = require('/code/net/packet')
local Util = require('/code/util')

args = {...}

-- Get coords from args
if args[3] == nil then
    print("Usage: gps_host.lua [x] [y] [z]")
    return
end

local coords = {
    x = tonumber(args[1]),
    y = tonumber(args[2]),
    z = tonumber(args[3])
}
local coords_packet = Packet:gps_resp(
    coords.x,
    coords.y,
    coords.z
)

function listener(packet, reply)
    print("Handling request from " .. packet.sender)
    reply(coords_packet)
end

function main()
    print("Responding to location requests using x = " .. coords.x .. ", y = " .. coords.y .. ", z = " .. coords.z)
end

local n = Network:create()
n:addListener(listener, Topics.GPS_QUERY)
n:listen(main)