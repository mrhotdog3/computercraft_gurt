BASE_URL = "https://bitbucket.org/mrhotdog3/computercraft_gurt/raw/master/src"

function downloadFile(filePath)
    local req = http.get(BASE_URL .. filePath)
    local file = fs.open(filePath, "w")
    file.write(req.readAll())
    file.close()
    req.close()
end

-- Download update script if necessary
if not fs.exists("/update.lua") then
    downloadFile("/update.lua")
end

-- Run update script
shell.run("update")

print("Finished updating")

-- Check for default service to run
service_file = "/service.start"
if fs.exists(service_file) then
    file = fs.open(service_file, "r")
    path = file.readAll()
    file.close()
    print("Executing default service \"" .. path .. "\"")
    shell.run(path)
end