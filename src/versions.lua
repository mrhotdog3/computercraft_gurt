VERSIONS = {
  code = {
    consts = "f6ce6e09c7858b79f158ae7e7c5acdec",
    gurt = "4d7a230e9afd354c6b6e540dc8e11654",
    inspect = "5bf360ee11b1c0f82c6123e63a62d68e",
    net = {
      channel = "1912d8817482f1a500447e30cb34f37d",
      gps = "2796423c3765d9fbdb837997849be00b",
      network = "d93aa7ba1827e1246e4866ee816f258d",
      packet = "1fe39a0fde24ee86d120b11f6afe0a76",
      topics = "b7c653a205157cf119a9773b550b81b5",
      wireless = "35effb913824ac0927ee6db20bae48c6"
    },
    persist = "3dfb8ddee6b0ad3d0b2fe95b3dabb451",
    pickle = "81753b20bf79eacf766553d1e215a945",
    polluted = {
      pickle = "358ea4a62ff262287b62107640ef16ad"
    },
    util = "3aa28403190bb1946c1577a99e86d5e4"
  },
  req = "b3710c2503634194b72270e0d73f708f",
  services = {
    farm = "28497bfa9f0fbd8f7521f6651fe6e6aa",
    gps_host = "450b656f49e581fe936a88016fcf47b1",
    grains = "1031ec99b832e76f9d93a4d0bc26db38",
    reactor_controller = "1df1d6f9734f5f8bd1444bd83af833ea"
  },
  startup = "897f4538b6338e9df8a64b5838d191ab",
  tests = {
    gps = "635116dd2cdac5c9abbabedbd0251d8c",
    move = "be8d90704a22945ac865126dc86ccaf7",
    quarry = {
      main = "594deb21204a0b41bad6651970964e08"
    },
    remotemine = "27ba7467642b4efa19a80eb7e5b74eaa",
    sniffer = "45afc17f98b572634f22ad65e702e2d7",
    test_pollution = "b441a7eef0f2673baf2f985c4f314e91",
    wireless_recv = "68745977a5126a444c78fe5fc2ed714f",
    wireless_send = "2b25645d4e195594725b58ca3f03ec60"
  },
  update = "0676a8d2126de5dc803e47014d417429"
}